# Firebase Authentication with NSB/AppStudio 6

- I will write a detailed tutorial here soon.



## This code is entirely in Javascript

- It took me a week to do this using other JavaScript frameworks.
- It took me a day todo this in NSB/AppStudio 6.
- I might translate this to basic code later if there is enough interest.



## Installation

Copy and paste your project's customized code snippet into `js/firebase.init.js`

Go to https://firebase.google.com/docs/web/setup for more information.

``` javascript
// Initialize Firebase
// TODO: Replace with your project's customized code snippet
var config = {
  apiKey: "<API_KEY>",
  authDomain: "<PROJECT_ID>.firebaseapp.com",
  databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
  projectId: "<PROJECT_ID>",
  storageBucket: "<BUCKET>.appspot.com",
  messagingSenderId: "<SENDER_ID>",
};
firebase.initializeApp(config);
```



## Things of Note...

1. In the `Project Explorer` click on `Project Properties and Global Code`.

1. In `Properties` scroll down to `extraheaders` where you will see...

``` html
<!-- Firebase App is always required and must be first -->
<script src="https://www.gstatic.com/firebasejs/5.4.1/firebase-app.js"></script>

<!-- Add additional services that you want to use -->
<script src="https://www.gstatic.com/firebasejs/5.4.1/firebase-auth.js"></script>
<!-- <script src="https://www.gstatic.com/firebasejs/5.4.1/firebase-database.js"></script> -->
<script src="https://www.gstatic.com/firebasejs/5.4.1/firebase-firestore.js"></script>
<!-- <script src="https://www.gstatic.com/firebasejs/5.4.1/firebase-messaging.js"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/5.4.1/firebase-functions.js"></script> -->

<!-- Firebase Initialization -->
<script src="js/firebase.init.js"></script>
```

I commented out some things I did not need, yet. I left them there because I might need them later.



## Changes/Commits

#### 2018.9.25 - Read From Firestore 

1. Added the ability to read data from Google Firebase Firestore. 

#### 2018.9.24 - Initial Commit

1. Added the ability to Authenticate a User from Google Firebase. 



## For More Information About NSB/AppStudio

Visit https://www.nsbasic.com/

